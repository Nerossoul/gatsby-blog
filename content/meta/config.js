module.exports = {
  siteTitle: "tzlaif", // <title>
  shortSiteTitle: "tzlaif", // <title> ending for posts and pages
  siteDescription: "tzlaif",
  siteUrl: "https://tzlaif.ru",
  // pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "ru",

  /* author */
  authorName: "tzlaif",
  authorTwitterAccount: "",
  authorGithub: "https://github.com/nerossoul",
  authorStackoverflow: "https://stackexchange.com/users/5678032/atte-juvonen?tab=accounts",
  authorLinkedin: "https://linkedin.com/in/atte-juvonen-86b886113",
  authorCodeforces: "https://codeforces.com/profile/baobab",
  authorYoutube: "https://www.youtube.com/channel/UCfHqfHoSYL0V_fXA_JQkeLw?view_as=subscriber",

  /* header text */
  headerTitle: "tzlaif",
  headerSubTitle: "",

  /* manifest.json */
  manifestName: "Atte Juvonen",
  manifestShortName: "atteJuvonen", // max 12 characters
  manifestStartUrl: "/index.html",
  manifestBackgroundColor: "white",
  manifestThemeColor: "#666",
  manifestDisplay: "standalone",

  // gravatar
  // Use your Gravatar image. If empty then will use src/images/jpg/avatar.jpg
  // Replace your email adress with md5-code.
  // Example https://www.gravatar.com/avatar/g.strainovic@gmail.com ->
  // gravatarImgMd5: "https://www.gravatar.com/avatar/1db853e4df386e8f699e4b35505dd8c6",
  gravatarImgMd5: ""
};
